module.exports = function(grunt) {
  var target = grunt.option('target') || '*';

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // paths
    meta: {
      assetPath_style: '../assets/styles/',
      assetPath_script: '../assets/scripts/',
      assetPath_script_lib: '../assets/scripts/lib/',
      assetPath_script_depend: '../assets/scripts/lib/dependencies/',
      assetPath_script_vendor: '../assets/scripts/lib/vendor/',
      assetPath_script_custom: '../assets/scripts/lib/custom',
      assetPath_image: '../assets/images/',
    },

    clean: {
      options: {
        force: true
      },
      build: ['../_styleguide/*']
    },

    // sass
    sass: {
      dist: {
        options: {
          style: 'compact',
          sourceMap: true
        },
        files: {
          '<%= meta.assetPath_style %>/style.css': '<%= meta.assetPath_style %>/scss/style.scss',
          '<%= meta.assetPath_style %>/wp-admin.css': '<%= meta.assetPath_style %>/scss/admin/wp-admin.scss',
          '<%= meta.assetPath_style %>/tinymce.css': '<%= meta.assetPath_style %>/scss/admin/tinymce.scss',
        },
      }
    },

    sass_globbing: {
      your_target: {
        files: {
          '<%= meta.assetPath_style %>/scss/_srcMaps/_settingsMap.scss': '<%= meta.assetPath_style %>/scss/settings/**/*.scss',
          '<%= meta.assetPath_style %>/scss/_srcMaps/_utilitiesMap.scss': '<%= meta.assetPath_style %>/scss/utilities/**/*.scss',
          '<%= meta.assetPath_style %>/scss/_srcMaps/_coreMap.scss': '<%= meta.assetPath_style %>/scss/core/**/*.scss',
          '<%= meta.assetPath_style %>/scss/_srcMaps/_globalMap.scss': '<%= meta.assetPath_style %>/scss/global/**/*.scss',
          '<%= meta.assetPath_style %>/scss/_srcMaps/_layoutsMap.scss': '<%= meta.assetPath_style %>/scss/layouts/**/*.scss',
          '<%= meta.assetPath_style %>/scss/_srcMaps/_componentsMap.scss': '<%= meta.assetPath_style %>/scss/components/**/*.scss',
          '<%= meta.assetPath_style %>/scss/_srcMaps/_pagesMap.scss': '<%= meta.assetPath_style %>/scss/pages/**/*.scss',
          '<%= meta.assetPath_style %>/scss/_srcMaps/_vendorMap.scss': '<%= meta.assetPath_style %>/scss/vendor/**/*.scss',
          '<%= meta.assetPath_style %>/scss/_srcMaps/_ampMap.scss': '<%= meta.assetPath_style %>/scss/amp/**/*.scss',

          tasks: ['sass_globbing'],
        },
        options: {
          useSingleQuotes: false
        }
      }
    },

    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({browsers: 'last 3 versions'}), // add vendor prefixes
          require('cssnano')() // minify the result
        ]
      },
      dist: {
        src: '<%= meta.assetPath_style %>/style.css'
      }
    },

    // concat js
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        src: ['<%= meta.assetPath_script_depend %>/*.js' , '<%= meta.assetPath_script_vendor %>/*.js' , '<%= meta.assetPath_script_custom %>/*.js'],
        dest: '<%= meta.assetPath_script %>/application.min.js'
      }
    },

    // uglify js
    uglify: {
      options: {
        banner: '',
        sourceMap: true
      },
      dist: {
        files: {
          '<%= meta.assetPath_script %>/application.min.js': ['<%= concat.dist.dest %>']
        }
      }
    },

    // optimize images
    imagemin: {
      png: {
        options: {
          optimizationLevel: 7
        },
        files: [
          {
            expand: true,
            cwd: '<%= meta.assetPath_image %>',
            src: ['**/*.png'],
            dest: '<%= meta.assetPath_image %>',
            ext: '.png'
          }
        ]
      },
      jpg: {
        options: {
          progressive: true
        },
        files: [
          {
            expand: true,
            cwd: '<%= meta.assetPath_image %>',
            src: ['**/*.jpg'],
            dest: '<%= meta.assetPath_image %>',
            ext: '.jpg'
          }
        ]
      }
    },

    // run browsersync
    browserSync: {
      dev: {
        bsFiles: {
          src : '<%= meta.assetPath_style %>/*.css'
        },
        options: {
          watchTask: true,
          host: "impact-dev.local/",
          proxy: 'impact-dev.local/'
        },
        ghostMode: {
          clicks: true,
          scroll: true,
          links: true,
          forms: true
        }
      }
    },

    // watch files
    watch: {
      scripts: {
        files: [
          '<%= meta.assetPath_script_lib %>/**/*.js'
        ],
        tasks: ['concat', 'uglify'],
      },
      css: {
        files: [
          '<%= meta.assetPath_style %>/scss/**/*.scss'
        ],
        tasks: ['sass_globbing', 'sass', 'postcss'],
      },
      images: {
        files: [
          '<%= meta.assetPath_image %>/**/*.*'
        ],
        tasks: ['imagemin'],
        options: {
          spawn: false,
        }
      }
    }
  });


  // plugins
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-sass-globbing');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-browser-sync');

  grunt.registerTask('default', ['browserSync', 'watch']);
};
