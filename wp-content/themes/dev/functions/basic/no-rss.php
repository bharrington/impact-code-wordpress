<?php
// no rss feed for static sites
function bph_disable_feed() {
  wp_die( __('No feed available,please visit our <a href="'. get_bloginfo('url') .'">homepage</a>!') );
}

add_action('do_feed', 'bph_disable_feed', 1);
add_action('do_feed_rdf', 'bph_disable_feed', 1);
add_action('do_feed_rss', 'bph_disable_feed', 1);
add_action('do_feed_rss2', 'bph_disable_feed', 1);
add_action('do_feed_atom', 'bph_disable_feed', 1);
