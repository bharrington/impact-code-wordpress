<?php
function bph_register_menus() {
  register_nav_menus(
    array(
    'menu__header' => __('Menu / Header'),
    'menu__contact' => __('Menu / Contact'),
    'menu__quick' => __('Menu / Quick Links'),
    )
  );
}

add_action('init', 'bph_register_menus');

// bem inspired custom walker
require_once dirname( __FILE__ ) . '/custom-menu/custom-walker.php';
