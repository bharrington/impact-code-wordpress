<?php
function bph_custom_images() {
  add_image_size( 'demo__xs', 250, 9999 );
  add_image_size( 'demo__sm', 450, 9999 );
  add_image_size( 'demo__md', 800, 9999 );
  add_image_size( 'demo__lg', 1000, 9999 );
  add_image_size( 'demo__xl', 1600, 9999 );
}

add_action( 'after_setup_theme', 'bph_custom_images' );
