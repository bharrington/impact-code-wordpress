<?php
add_action( 'init', 'bph_add_editor_styles' );
function bph_add_editor_styles() {
  add_editor_style( get_template_directory_uri() . '/assets/styles/style.css' );
}
