<?php
add_action('admin_head', 'bph_custom_admin_styles');
function bph_custom_admin_styles() {
  echo '<link rel="stylesheet" type="text/css" href="'.get_template_directory_uri().'/assets/styles/wp-admin.css">';
  echo '<script src="'.get_template_directory_uri().'/assets/scripts/wp-admin.js"></script>';
}
