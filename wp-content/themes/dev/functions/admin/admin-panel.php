<?php
// welcome for all users
add_filter('user_has_cap', 'bph_user_has_cap');
 function bph_user_has_cap($capabilities){
 global $pagenow;
    if ($pagenow == 'index.php')
         $capabilities['edit_theme_options'] = 1;
    return $capabilities;
 }

function show_welcome_panel() {
  $user_id = get_current_user_id();

  if ( 1 != get_user_meta( $user_id, 'show_welcome_panel', true ) )
      update_user_meta( $user_id, 'show_welcome_panel', 1 );
}

// Welcome Panel
add_filter("get_user_metadata", "bph_welcome_panel_custom", 1, 4);
function bph_welcome_panel_custom($null, $object_id, $meta_key, $single) {
// Only work with the show_welcome_panel
if($meta_key !== 'show_welcome_panel') { return null; }

$url = get_option('siteurl');
$homeurl = get_option('home');
$title = get_option('blogname');
$description = get_option('blogdescription');

 echo '

<div class="welcome-panel" style="padding-bottom: 23px">

  <div class="welcome-panel-content">
    <h2>'.$title.'</h2>
    <p class="about-description">Some links to get you started:</p>

    <div class="welcome-panel-column-container">

      <div class="welcome-panel-column  style="padding-right: 25px;"">
        <h3>Get Started</h3>
        <p>First, make sure you go and customize the site/theme options.</p>
        <a class="button button-primary button-hero load-customize hide-if-no-customize" href="' .$url .'/wp-admin/admin.php?page=theme_options">Customize Your Site</a>
      </div>

      <div class="welcome-panel-column">
        <h3>Next Steps</h3>
        <ul>
          <li><a class="welcome-icon welcome-add-page" href="'. $url . '/wp-admin/post-new.php?post_type=page">Add a Page</a></li>
          <li><a class="welcome-icon welcome-add-page" href="http://impact-demo.tk/our-products/">View Site</a></li>
          <li><a class="welcome-icon welcome-add-page" href="http://amp.impact-demo.tk/">View Google AMP version</a></li>
        </ul>
      </div>

      <div class="welcome-panel-column">
        <h3>Curious</h3>
        <p>See how I tackled this sample project.</p>
        <a class="button button-primary button-hero load-customize hide-if-no-customize" href="https://bitbucket.org/bharrington/impact-code-wordpress/">View Repo</a>

        <a class="button button-primary button-hero load-customize hide-if-no-customize" href="https://drive.google.com/open?id=1ALJCMec1HCrsHWOJh-0FUmwVaFQNz4ru">View PSD Mockup</a>
      </div>

    </div>

  </div>
</div>
 ';

// Return 0 or else the original welcome panel will show as well.
return 0;
}
