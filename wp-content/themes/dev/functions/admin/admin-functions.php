<?php
// Custom Backend Footer
function bph_custom_admin_footer() {
  _e('<span id="footer-thankyou">Developed by <a href="mailto:brianpeterharrington@hotmail.com" target="_blank">Brian Harrington</a></span>.', 'bph');
}
add_filter('admin_footer_text', 'bph_custom_admin_footer');

// Login css
function bph_login_css() {
  wp_enqueue_style( 'bph_login_css', get_template_directory_uri() . '/assets/styles/style-login.css', false );
}
add_action( 'login_enqueue_scripts', 'bph_login_css', 10 );

// changing the logo link from wordpress.org to your site
function bph_login_url() {  return home_url(); }
add_filter('login_headerurl', 'bph_login_url');

// changing the alt text on the logo to show your site name
function bph_login_title() { return get_option('blogname'); }
add_filter('login_headertitle', 'bph_login_title');

// remove title
remove_action( 'wp_head', '_wp_render_title_tag', 1 );
