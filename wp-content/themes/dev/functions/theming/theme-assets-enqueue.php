<?php
// Dereg GF scripts
add_action("gform_enqueue_scripts", "bph_deregister_scripts");
function bph_deregister_scripts(){
  wp_deregister_script("jquery");
}

// Load vendor js
if( !is_admin()){
  function bph_enqueue_scripts() {
    wp_deregister_script('jquery');
    wp_register_script('jquery', ("//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"), array(), '1.10.2', true);
    wp_enqueue_script('jquery');
  }

  add_action('wp_enqueue_scripts', 'bph_enqueue_scripts');
}

// Load theme js & css
add_action( 'wp_enqueue_scripts', 'bph_theme_js_css' );
function bph_theme_js_css() {
  wp_enqueue_style( 'style-name', get_template_directory_uri() . '/assets/styles/style.css', array(), filemtime(get_stylesheet_directory() . '/assets/styles/style.css') );
  wp_enqueue_script( 'application', get_template_directory_uri() . '/assets/scripts/application.min.js', array(), filemtime(get_stylesheet_directory() . '/assets/scripts/application.min.js'), true );
}
