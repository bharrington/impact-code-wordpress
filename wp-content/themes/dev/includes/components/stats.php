<?php
/**
 * name: Stats
 * usage: Comparison stat module
 * scss: assets/styles/components/stats.scss
 * js: assets/scripts/lib/custom/stats.js
 * dependencies: global jquery, assets/scripts/vendor/jquery.waypoints.js, assets/scripts/vendor/jquery.once.js
 */
?>

<?php $stats       = get_sub_field('stats--group');
      $stats_count = count(get_sub_field('stat_item')); ?>

<div class="stats">
  <div class="stats__wrap">
    <?php if ( $stats['title'] ): ?>
      <h2 class="stats__title"><?php echo $stats['title']; ?></h2>
    <?php endif; ?>

    <?php if ( $stats['text'] ): ?>
      <p class="stats__text"><?php echo $stats['text']; ?></p>
    <?php endif; ?>

    <?php if( have_rows('stat_item') ): ?>
      <div class="stats__group has-<?php echo $stats_count; ?>">
        <?php while ( have_rows('stat_item') ) : the_row(); ?>
          <?php if( get_row_layout() == 'item' ): ?>
            <?php
              $value   = get_sub_field('value');
              $prefix  = get_sub_field('prefix');
              $suffix  = get_sub_field('suffix');
              $measure = get_sub_field('measure'); ?>

              <div class="stats__item">
                <?php if ( $value ): ?>
                <h2 class="stats__item-value"><?php echo $prefix ?><span data-stat-number><?php echo $value; ?></span><?php echo $suffix ?></h2>
                <?php endif; ?>

                <?php if ( $measure ): ?>
                  <h4 class="stats__item-measure"><?php echo $measure; ?></h4>
                <?php endif; ?>
              </div>
          <?php endif; ?>
        <?php endwhile; ?>
      </div>
    <?php endif; ?>

    <?php if ( $stats['sub_text'] ): ?>
      <p class="stats__subtext"><?php echo $stats['sub_text']; ?></p>
    <?php endif; ?>
  </div>
</div>

