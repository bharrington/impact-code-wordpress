<?php
/**
 * name: CTA
 * usage: Multi use call to actions; large & slim formats
 * scss: assets/styles/components/cta.scss
 * js: assets/scripts/lib/custom/cta.js
 * dependencies: global jquery
 */
?>

<?php $cta          = get_sub_field('cta--group');
      $query_string = $cta['query_string'];

      if ( $query_string ) {
        $query_string = '?'.$query_string;
        $query_string = preg_replace('/\s+/', '_', $query_string);
      } ?>

<div class="cta <?php if ( $cta['image'] ): ?>has-image<?php endif; ?>"<?php if ( $cta['image'] ): ?> data-cta<?php endif; ?>>
  <div class="cta__wrap">
    <?php if ( $cta['title'] && $cta['image'] ): ?>
      <h1 class="cta__title"><?php echo $cta['title']; ?></h1>
    <?php elseif ( $cta['title'] ): ?>
      <h2 class="cta__title"><?php echo $cta['title']; ?></h2>
    <?php endif; ?>

    <?php if ( $cta['subtext'] && $cta['image'] ): ?>
      <h3 class="cta__text"><?php echo $cta['subtext']; ?></h3>
    <?php elseif( $cta['subtext'] ): ?>
      <h4 class="cta__text"><?php echo $cta['subtext']; ?></h4>
    <?php endif; ?>

    <?php if ( $cta['button-text'] && $cta['button-link'] ): ?>
      <a class="btn" href="<?php echo $cta['button-link'] ?><?php echo $query_string ?>"><?php echo $cta['button-text']; ?></a>
    <?php endif; ?>

    <?php if ( $cta['image'] ): ?>
      <div class="cta__image" data-cta-image>
        <img src="<?php echo $cta['image']['sizes']['demo__md']; ?>">
      </div>
    <?php endif; ?>
  </div>
</div>
