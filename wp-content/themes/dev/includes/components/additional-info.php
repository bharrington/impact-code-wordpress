<?php
/**
 * name: Additional Info
 * usage: Basic blocks that max at 3.
 * scss: assets/styles/components/additional-info.scss
 */
?>

<?php $title      = get_sub_field('title');
      $info_count = count(get_sub_field('additional-info__group')); ?>

<div class="additional-info">
  <?php if ( $title ): ?>
    <h2 class="additional-info__title"><?php echo $title; ?></h2>
  <?php endif; ?>

  <?php if( have_rows('additional-info__group') ): ?>
  <div class="additional-info__group has-<?php echo $info_count; ?>">
    <?php while ( have_rows('additional-info__group') ) : the_row(); ?>
      <?php if( get_row_layout() == 'item' ): ?>
        <?php
          $image  = get_sub_field('icon');
          $image_ = $image['sizes']['demo__xs'];
          $title  = get_sub_field('title');
          $text   = get_sub_field('text'); ?>

          <div class="additional-info__item <?php if ( $image ): ?>has-image<?php endif; ?>">

            <?php if ( $image ): ?>
              <div class="additional-info__item-image">
                <img src="<?php echo $image_; ?>" />
              </div>
            <?php endif; ?>

            <?php if ( $title ): ?>
              <h4 class="additional-info__item-title"><?php echo $title; ?></h4>
            <?php endif; ?>

            <?php if ( $text ): ?>
              <p class="additional-info__item-text"><?php echo $text; ?></p>
            <?php endif; ?>

          </div>
      <?php endif; ?>
    <?php endwhile; ?>
  </div>
  <?php endif; ?>
</div>
