<?php
/**
 * name: Testimonials
 * usage: Simple testimonial slider
 * scss: assets/styles/components/testimonials.scss
 * js: assets/scripts/lib/custom/testimonials.js
 * dependencies: owl-carousel : assets/scripts/lib/vendor/owl.carousel.min.js
 */
?>

<?php $testimonials       = get_sub_field('testimonials--group');
      $testimonials_count = count(get_sub_field('testimonials')); ?>

<div class="testimonial">
  <?php if ( $testimonials['title'] ): ?>
    <h2 class="testimonial__title"><?php echo $testimonials['title']; ?></h2>
  <?php endif; ?>

  <?php if( have_rows('testimonials') ): ?>
  <div class="testimonial__group owl-carousel owl-theme has-<?php echo $testimonials_count; ?>" data-testimonials>
    <?php while ( have_rows('testimonials') ) : the_row(); ?>
      <?php if( get_row_layout() == 'item' ): ?>
        <?php
          $profile  = get_sub_field('image');
          $profile_ = $profile['sizes']['demo__xs'];
          $name     = get_sub_field('name');
          $position = get_sub_field('position');
          $company  = get_sub_field('company');
          $quote    = get_sub_field('quote'); ?>

          <div class="testimonial__item <?php if ( $profile ): ?>has-image<?php endif; ?>">
            <?php if ( $quote ): ?>
              <blockquote class="testimonial__item-quote"><?php echo $quote; ?></blockquote>
            <?php endif; ?>

            <?php if ( $profile ): ?>
              <div class="testimonial__item-profile">
                <img src="<?php echo $profile_; ?>" />
              </div>
            <?php endif; ?>

            <cite class="testimonial__item-info">
              <?php if ( $name ): ?>
                <span><?php echo $name; ?></span>
              <?php endif; ?>

              <?php if ( $position ): ?>
                <span><?php echo $position; ?></span>
              <?php endif; ?>

              <?php if ( $company ): ?>
                <span><?php echo $company; ?></span>
              <?php endif; ?>
            </cite>
          </div>
      <?php endif; ?>
    <?php endwhile; ?>
  </div>
  <?php endif; ?>
</div>
