<?php
/**
 * name: Features
 * usage: Used to help describe the features of the of product with visual lists
 * scss: assets/styles/components/features.scss
 */
?>

<?php $features = get_sub_field('features--group'); ?>

<div class="features">
  <div class="features__wrap">
    <div class="features__content">
      <?php if ( $features['prefix'] ): ?>
        <h5 class="features__prefix"><?php echo $features['prefix']; ?></h5>
      <?php endif; ?>

      <?php if ( $features['title'] ): ?>
        <h3 class="features__title"><?php echo $features['title']; ?></h3>
      <?php endif; ?>

      <?php if ( $features['text'] ): ?>
        <div class="features__text">
          <p><?php echo $features['text']; ?></p>
        </div>
      <?php endif; ?>

      <?php if( have_rows('features') ): ?>
      <div class="features__group">
        <?php while ( have_rows('features') ) : the_row(); ?>
          <?php if( get_row_layout() == 'item' ): ?>
            <?php
              $image  = get_sub_field('icon');
              $image_ = $image['sizes']['demo__xs'];
              $title  = get_sub_field('title');
              $text   = get_sub_field('text'); ?>

              <div class="features__item <?php if ( $image ): ?>has-image<?php endif; ?>">

                <?php if ( $image ): ?>
                  <div class="features__item-image">
                    <img src="<?php echo $image_; ?>" />
                  </div>
                <?php endif; ?>

                <?php if ( $title || $text ): ?>
                  <div class="features__item-content">
                    <?php if ( $title ): ?>
                      <h4 class="features__item-title"><?php echo $title; ?></h4>
                    <?php endif; ?>

                    <?php if ( $text ): ?>
                      <p class="features__item-text"><?php echo $text; ?></p>
                    <?php endif; ?>
                  </div>
                <?php endif; ?>
              </div>
          <?php endif; ?>
        <?php endwhile; ?>
      </div>
      <?php endif; ?>
    </div>
  </div>
</div>
