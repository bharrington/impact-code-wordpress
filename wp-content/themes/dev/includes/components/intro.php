<?php
/**
 * name: Intro
 * usage: Simple intro text and optional image
 * scss: assets/styles/components/intro.scss
 */
?>

<?php $intro     = get_sub_field('intro--group');
      $intro_pos = get_sub_field('intro--text-pos'); ?>

<div class="intro">
  <?php if ( $intro['title'] ): ?>
    <h2 class="intro__title"><?php echo $intro['title']; ?></h2>
  <?php endif; ?>

  <div class="intro__wrap text-is-<?php echo $intro_pos; ?>">
    <?php if ( $intro['text'] ): ?>
      <div class="intro__text"><?php echo $intro['text']; ?></div>
    <?php endif; ?>

    <?php if ( $intro['image'] ): ?>
      <div class="intro__image">
        <img src="<?php echo $intro['image']['sizes']['demo__md']; ?>">
      </div>
    <?php endif; ?>
  </div>
</div>
