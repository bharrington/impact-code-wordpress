  <footer class="footer" role="contentinfo">
    <div class="footer__wrap">
      <p class="footer__logo"><?php bloginfo('name'); ?></p>

      <div class="footer__menus">
        <nav role="navigation" class="menu--contact">
          <h5>Contact</h5>
          <?php bem_menu('menu__contact', 'contact', '', '1'); ?>
        </nav>

        <nav role="navigation" class="menu--quick-links">
          <h5>Quick Links</h5>
          <?php bem_menu('menu__quick', 'quick-link', '', '1'); ?>
        </nav>
      </div>
    </div>

    <p class="footer__copyright">Copyright <?php echo date('Y'); ?> <?php bloginfo('name'); ?>. All Rights Reserved.</p>
  </footer>

  <?php wp_footer(); ?>

  <?php if( get_field('google_analytics', 'option') ): ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php the_field('google_analytics', 'option'); ?>"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', '<?php the_field('google_analytics', 'option'); ?>');
    </script>
  <?php endif; ?>
</body>
</html>
