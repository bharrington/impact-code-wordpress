<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<?php if( get_field('seo_alt_title') ): ?>
  <title><?php bloginfo('name'); ?> - <?php the_field('seo_alt_title'); ?></title>
<?php else: ?>
  <title><?php bloginfo('name'); ?> - <?php the_title(); ?></title>
<?php endif; ?>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php if( is_front_page() ): ?>
  <meta name="description" content="<?php bloginfo('description'); ?>" />
<?php else: ?>
  <meta name="description" content="<?php the_field('seo_meta_description'); ?>" />
<?php endif; ?>
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <header class="header" role="banner">
    <div class="header__wrap">
      <a href="<?php echo home_url(); ?>"><h1 class="header__logo"><?php bloginfo('name'); ?></h1></a>
      <i class="menu--trigger" data-reveal-menu>Menu</i>

      <div class="menu__wrap --is-hidden" data-menu-wrap>
        <i class="menu--close" data-close-menu>Close</i>

        <nav role="navigation" class="menu--main" data-menu-main>
          <?php bem_menu('menu__header', 'menu', '', '2'); ?>
        </nav>
      </div>
    </div>
  </header>
