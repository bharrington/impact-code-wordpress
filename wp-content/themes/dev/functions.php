<?php
////////////////////////
/// Theming Functions
////////////////////////
require_once ('functions/theming/theme-assets-enqueue.php');
require_once ('functions/theming/html-minify.php');
require_once ('functions/theming/theme-support.php');

////////////////////////
/// Basic Functions
////////////////////////
require_once ('functions/basic/security.php');
require_once ('functions/basic/remove-plugin-update-nag.php');
require_once ('functions/basic/no-rss.php');

////////////////////////
/// Admin Functions
////////////////////////
require_once ('functions/admin/wp-admin-menu-classes.php');
require_once ('functions/admin/editor-style.php');
require_once ('functions/admin/admin-panel.php');
require_once ('functions/admin/admin-style.php');
require_once ('functions/admin/admin-functions.php');
require_once ('functions/admin/admin-cleanup.php');
require_once ('functions/admin/no-theme-edit.php');
require_once ('functions/admin/hide-admin-menu.php');

////////////////////////
/// Customizations
////////////////////////
require_once ('functions/custom/custom-functions.php');
require_once ('functions/custom/custom-fields.php');
require_once ('functions/custom/custom-image-sizes.php');
require_once ('functions/custom/custom-menus.php');
require_once ('functions/custom/custom-posts.php');

////////////////////////
/// More Customizations
////////////////////////
require_once ('functions/custom/custom-tinymce-base-edit.php');
require_once ('functions/custom/custom-tinymce-items.php');
require_once ('functions/custom/custom-body-class.php');
require_once ('functions/theming/theme-typekit.php');
