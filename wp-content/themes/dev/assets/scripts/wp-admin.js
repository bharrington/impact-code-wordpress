jQuery(document).ready(function($){
  function add_column_width_to_heading() {
    $('.widthHelper').remove();
    $('.acf-field-56bd005663e12').each(function() {
      var width = $(this).find('.acf-input select option:selected').text();
      var width = "<span class='widthHelper'> - " + width + "</span>";
      $(this).parent().siblings('.acf-fc-layout-handle').append(width);
    });
  }

  $(window).load(function() {
    add_column_width_to_heading();
  });

  $('.acf-fc-layout-handle').click(function() {
    function show_popup(){
      add_column_width_to_heading();
    };

    window.setTimeout( show_popup, 700 );
  });

  $(document).on('change', '.acf-field-56bd005663e12 select', function() {
    add_column_width_to_heading();
  });
});
