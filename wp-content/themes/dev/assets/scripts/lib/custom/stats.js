/**
 * measures cta image if it exisits and then makes adjustments center in div
 *
 */

(function(){
  var statCount = once(function() {
    $('[data-stat-number]').each(function () {
      var $this = $(this);
      $({ Counter: 0 }).animate({ Counter: $this.text() }, {
        duration: 1500,
        easing: 'swing',
        step: function () {
          $this.text(Math.ceil(this.Counter));
        }
      });
    });
  });

  var w = window.innerHeight - 20;

  // on appear
  var waypoint = new Waypoint({
    element: $('.stats')[0],
    handler: function(direction) {
      statCount();
    },
    offset: w
  });
})();
