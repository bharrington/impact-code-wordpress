/**
 * measures cta image if it exisits and then makes adjustments center in div
 *
 */

(function(){
  var timeout = false;

  var ctaImage = function() {
    var ctaImage       = $('[data-cta-image]'),
        ctaImageHeight = $('[data-cta-image]').height(),
        ctaWrap        = $('[data-cta]');

    if( $(window).width() > 700) { // adjust in utilities/_settings.scss
      ctaImage.css('margin-bottom', -(ctaImageHeight / 2));
      ctaWrap.css('margin-bottom', (ctaImageHeight / 2));
    } else {
      ctaImage.css('margin-bottom', '');
      ctaWrap.css('margin-bottom', '');
    }
  }

  // init
  ctaImage();

  // resize
  window.addEventListener('resize', function() {
    clearTimeout(timeout);
    timeout = setTimeout(ctaImage(), 100);
  });
})();
