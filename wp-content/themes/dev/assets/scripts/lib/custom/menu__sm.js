/**
 * Main menu triggering
 *
 */

(function(){
  // remove linkage for parent menu items - header
  $( '.menu__item--parent > .menu__link' ).each( function() {
    $( this ).removeAttr( 'href' );
    $( this ).attr('data-menu-parent', '');
  });

  // open, close, etc
  var body         = $( 'body' ),
      menu_trigger = $( '[data-reveal-menu]' ),
      menu_close   = $( '[data-close-menu]' ),
      menu_wrap    = $( '[data-menu-wrap]' );

  menu_trigger.click(function() {
    body.toggleClass( '--menu-is-active' );
    menu_trigger.toggleClass( '--is-active' );
    menu_wrap.toggleClass( '--menu-visible' );
    menu_wrap.removeClass( 'animated --is-hidden' );
    menu_wrap.addClass( 'animated fadeIn' );
  });

  menu_close.click(function() {
    body.toggleClass( '--menu-is-active' );
    menu_trigger.toggleClass( '--is-active' );
    menu_wrap.toggleClass( '--menu-visible' );
    menu_wrap.removeClass( 'animated fadeIn' );

    // delay add class back
    menu_wrap.addClass( '--is-hidden' );
  });

  // second level reveal
  var sub_menu_trigger = $( '[data-menu-parent]' ),
      sub_menus        = $( '.menu__sub-menu' );

  sub_menu_trigger.click(function() {
    if ( $( this ).siblings( '.menu__sub-menu' ).hasClass( '--show-sub-menu' ) ) {
      sub_menus.removeClass( '--show-sub-menu' );
    } else {
      sub_menus.removeClass( '--show-sub-menu' );
      $( this ).siblings( '.menu__sub-menu' ).toggleClass( '--show-sub-menu' );
    }
  });
})();
