/**
 *   Slider
 */

(function(){
  var testimonialSlider;
  testimonialSlider = $("[data-testimonials]").owlCarousel({
    nav: true,
    items: 1,
    dots: true,
    autoHeight: true,
  });
})();
