<?php get_template_part( 'includes/global/header' ); ?>

<div class="error">
  <div>
    <h1>404 Error</h1>
    <p>Please try again.</p>
  </div>
</div>

<?php get_template_part( 'includes/global/footer' ); ?>
