<?php
/**
 * Template Name: Landing Page
 */
?>

<?php get_template_part( 'includes/global/header' ); ?>

<?php if( get_field('layouts') ):
  while ( has_sub_field('layouts') ) :
    $rowName = get_row_layout();
    include(dirname(__FILE__) . '../../includes/components/'.$rowName.'.php');
  endwhile;
endif;?>

<?php get_template_part( 'includes/global/footer' ); ?>
